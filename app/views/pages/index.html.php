
<?php ob_start();

?>


	<!-- Home -->

	<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=ROOT_URL?>public/images/home_image.jpg" data-speed="0.8"></div>
		<div class="home_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_title text_center">
					<img class="logo_2" src="<?=ROOT_URL?>public/images/simbad_logo_new.png" alt="">
				<h1>Reserve con nosotros directamente al mejor precio</h1></div>
			<div class="home_text text-center"></div>
			<div class="button home_button"><a href="#">book now</a></div>
		</div>
	</div>


    
    <!-- Intro -->

	<div class="intro">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<div>Welcome</div>
						<h1>Amazing Hotel in front of the Sea</h1>
					</div>
				</div>
			</div>
			<div class="row intro_row">
				<div class="col-xl-8 col-lg-10 offset-xl-2 offset-lg-1">
					<div class="intro_text text-center">
						<p>
							Welcome to the hotel Simbad
							Our hotel on the beachfront of Talamanca and open all year round with 24-hour reception service has 111 rooms at your disposal.
							
							You can book comfortably in four simple steps and get advantages.
							
						<ul>
							<li>Open all year long</li>
							<li>24 hour reception service.</li>
							<li>A la carte restaurant.</li>
							<li>Brunch every Sunday.</li>
							<li>Weddings and communions.</li>
							<li>Company events</li>
						</ul>
						</p>
					</div>
				</div>
			</div>
			<div class="row gallery_row">
				<div class="col">

					<!-- Gallery -->
					<div class="gallery_slider_container">
						<div class="owl-carousel owl-theme gallery_slider">
							
							<!-- Slide -->
							<div class="gallery_slide">
								<img src="<?=ROOT_URL?>public/images/gallery_1.jpg" alt="">
								<div class="gallery_overlay">
									<div class="text-center d-flex flex-column align-items-center justify-content-center">
										<a href="#">
											<span>+</span>
											<span>See More</span>
										</a>
									</div>
								</div>
							</div>

							<!-- Slide -->
							<div class="gallery_slide">
								<img src="<?=ROOT_URL?>public/images/gallery_2.jpg" alt="">
								<div class="gallery_overlay">
									<div class="text-center d-flex flex-column align-items-center justify-content-center">
										<a href="#">
											<span>+</span>
											<span>See More</span>
										</a>
									</div>
								</div>
							</div>

							<!-- Slide -->
							<div class="gallery_slide">
								<img src="<?=ROOT_URL?>public/images/gallery_3.jpg" alt="">
								<div class="gallery_overlay">
									<div class="text-center d-flex flex-column align-items-center justify-content-center">
										<a href="#">
											<span>+</span>
											<span>See More</span>
										</a>
									</div>
								</div>
							</div>

							<!-- Slide -->
							<div class="gallery_slide">
								<img src="<?=ROOT_URL?>public/images/gallery_4.jpg" alt="">
								<div class="gallery_overlay">
									<div class="text-center d-flex flex-column align-items-center justify-content-center">
										<a href="#">
											<span>+</span>
											<span>See More</span>
										</a>
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


  
  <?php $content = ob_get_clean() ?>
  <?php include 'app/views/layout.html.php' ?>