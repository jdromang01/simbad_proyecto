
<?php ob_start();
print_r($data['posts']);


?>


<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/blog.jpg" data-speed="0.8"></div>
		<div class="home_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_title"><h1>Blog</h1></div>
		</div>
    </div>
    
    <!-- Blog -->

	<div class="blog">
		<div class="container">
			<div class="row">
				
				<!-- Blog -->
				<div class="col-lg-9">
					<div class="blog_posts">
						
						<!-- Blog Post -->
						<div class="blog_post">
							<div class="blog_post_image">
								<div><img src="images/blog_1.jpg" alt=""></div>
								<div class="blog_post_date">
									<a href="#">
										<span class="d-flex flex-column align-items-center justify-content-center">
											<span>
												<span>27</span>
												<span>Dec'18</span>
											</span>
										</span>
									</a>
								</div>
							</div>
							<div class="blog_post_content">
								<div class="blog_post_title"><h2><a href="#">How to plan the perfect vacation</a></h2></div>
								<div class="blog_post_tags">
									<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
										<li><a href="#">Vacation</a></li>
										<li><a href="#">Trip</a></li>
										<li><a href="#">Hotel</a></li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p>Maecenas sollicitudin tincidunt maximus. Morbi tempus malesuada erat sed pellentesque. Donec pharetra mattis nulla, id laoreet neque scelerisque at. Quisque eget sem non ligula consectetur ultrices in quis augue. Donec imperd iet leo eget tortor dictum, eget varius eros sagittis.</p>
								</div>
								<div class="button blog_post_button"><a href="#">read more</a></div>
							</div>
						</div>

						<!-- Blog Post -->
						<div class="blog_post">
							<div class="blog_post_image">
								<div><img src="images/blog_2.jpg" alt=""></div>
								<div class="blog_post_date">
									<a href="#">
										<span class="d-flex flex-column align-items-center justify-content-center">
											<span>
												<span>27</span>
												<span>Dec'18</span>
											</span>
										</span>
									</a>
								</div>
							</div>
							<div class="blog_post_content">
								<div class="blog_post_title"><h2><a href="#">Do you want a destination wedding?</a></h2></div>
								<div class="blog_post_tags">
									<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
										<li><a href="#">Vacation</a></li>
										<li><a href="#">Trip</a></li>
										<li><a href="#">Hotel</a></li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p>Maecenas sollicitudin tincidunt maximus. Morbi tempus malesuada erat sed pellentesque. Donec pharetra mattis nulla, id laoreet neque scelerisque at. Quisque eget sem non ligula consectetur ultrices in quis augue. Donec imperd iet leo eget tortor dictum, eget varius eros sagittis.</p>
								</div>
								<div class="button blog_post_button"><a href="#">read more</a></div>
							</div>
						</div>

						<!-- Blog Post -->
						<div class="blog_post">
							<div class="blog_post_image">
								<div><img src="images/blog_3.jpg" alt=""></div>
								<div class="blog_post_date">
									<a href="#">
										<span class="d-flex flex-column align-items-center justify-content-center">
											<span>
												<span>27</span>
												<span>Dec'18</span>
											</span>
										</span>
									</a>
								</div>
							</div>
							<div class="blog_post_content">
								<div class="blog_post_title"><h2><a href="#">Plan your vacation on time</a></h2></div>
								<div class="blog_post_tags">
									<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
										<li><a href="#">Vacation</a></li>
										<li><a href="#">Trip</a></li>
										<li><a href="#">Hotel</a></li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p>Maecenas sollicitudin tincidunt maximus. Morbi tempus malesuada erat sed pellentesque. Donec pharetra mattis nulla, id laoreet neque scelerisque at. Quisque eget sem non ligula consectetur ultrices in quis augue. Donec imperd iet leo eget tortor dictum, eget varius eros sagittis.</p>
								</div>
								<div class="button blog_post_button"><a href="#">read more</a></div>
							</div>
						</div>

					</div>

					<!-- Page Nav -->
					<div class="page_nav">
						<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
							<li class="active"><a href="#">01.</a></li>
							<li><a href="#">02.</a></li>
							<li><a href="#">03.</a></li>
						</ul>
					</div>

				</div>

				<!-- Sidebar -->
				<div class="col-lg-3">
					<div class="sidebar">
						
						<!-- Recent Posts -->
						<div class="recent_posts">
							<div class="sidebar_title"><h3>Recent Posts</h3></div>
							<div class="recent_posts_container">
								
								<!-- Recent Post -->
								<div class="post_small d-flex flex-row align-items-center justify-content-start">
									<div class="post_small_image"><a href="#"><img src="images/recent_1.jpg" alt=""></a></div>
									<div class="post_small_content">
										<div class="post_small_title"><a href="#">Seasonal Offers</a></div>
										<div class="post_small_date"><a href="#">March 05, 2018</a></div>
									</div>
								</div>

								<!-- Recent Post -->
								<div class="post_small d-flex flex-row align-items-center justify-content-start">
									<div class="post_small_image"><a href="#"><img src="images/recent_2.jpg" alt=""></a></div>
									<div class="post_small_content">
										<div class="post_small_title"><a href="#">Why you need to visit Bali?</a></div>
										<div class="post_small_date"><a href="#">February 11, 2018</a></div>
									</div>
								</div>

								<!-- Recent Post -->
								<div class="post_small d-flex flex-row align-items-center justify-content-start">
									<div class="post_small_image"><a href="#"><img src="images/recent_3.jpg" alt=""></a></div>
									<div class="post_small_content">
										<div class="post_small_title"><a href="#">A good selection of vines from Italy</a></div>
										<div class="post_small_date"><a href="#">January 25, 2018</a></div>
									</div>
								</div>

								<!-- Recent Post -->
								<div class="post_small d-flex flex-row align-items-center justify-content-start">
									<div class="post_small_image"><a href="#"><img src="images/recent_4.jpg" alt=""></a></div>
									<div class="post_small_content">
										<div class="post_small_title"><a href="#">Traveling with kids?</a></div>
										<div class="post_small_date"><a href="#">January 05, 2018</a></div>
									</div>
								</div>

							</div>
						</div>

						<!-- Categories -->
						<div class="categories">
							<div class="sidebar_title"><h3>Categories</h3></div>
							<div class="categories_list">
								<ul>
									<li><a href="#">Our World</a></li>
									<li><a href="#">Travel & Living</a></li>
									<li><a href="#">Entertainment</a></li>
									<li><a href="#">Food & Restaurants</a></li>
									<li><a href="#">Uncategorized</a></li>
								</ul>
							</div>
						</div>

						<!-- Tabs -->
						<div class="tabs">
							<div class="tabs_container">
								<div class="tabs d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div class="tab active">Popular</div>
									<div class="tab">Comments</div>
								</div>
								<div class="tab_panels">

									<div class="tab_panel active">
										<div class="tab_panel_content">
											
											<!-- Popular -->
											<div class="popular">

												<!-- Popular Post -->
												<div class="post_small d-flex flex-row align-items-center justify-content-start">
													<div class="post_small_image"><a href="#"><img src="images/recent_5.jpg" alt=""></a></div>
													<div class="post_small_content">
														<div class="post_small_title"><a href="#">Mediteranean Atractions</a></div>
														<div class="post_small_date"><a href="#">December 03, 2018</a></div>
													</div>
												</div>

												<!-- Popular Post -->
												<div class="post_small d-flex flex-row align-items-center justify-content-start">
													<div class="post_small_image"><a href="#"><img src="images/recent_6.jpg" alt=""></a></div>
													<div class="post_small_content">
														<div class="post_small_title"><a href="#">Choose your stay</a></div>
														<div class="post_small_date"><a href="#">December 11, 2018</a></div>
													</div>
												</div>

												<!-- Popular Post -->
												<div class="post_small d-flex flex-row align-items-center justify-content-start">
													<div class="post_small_image"><a href="#"><img src="images/recent_7.jpg" alt=""></a></div>
													<div class="post_small_content">
														<div class="post_small_title"><a href="#">Time pass atractions</a></div>
														<div class="post_small_date"><a href="#">December 05, 2018</a></div>
													</div>
												</div>

											</div>
										</div>
									</div>

									<div class="tab_panel">
										<div class="tab_panel_content">
											
											<!-- Popular -->
											<div class="popular">

												<!-- Popular Post -->
												<div class="post_small d-flex flex-row align-items-center justify-content-start">
													<div class="post_small_image"><a href="#"><img src="images/recent_5.jpg" alt=""></a></div>
													<div class="post_small_content">
														<div class="post_small_title"><a href="#">Mediteranean Atractions</a></div>
														<div class="post_small_date"><a href="#">December 03, 2018</a></div>
													</div>
												</div>

												<!-- Popular Post -->
												<div class="post_small d-flex flex-row align-items-center justify-content-start">
													<div class="post_small_image"><a href="#"><img src="images/recent_6.jpg" alt=""></a></div>
													<div class="post_small_content">
														<div class="post_small_title"><a href="#">Choose your stay</a></div>
														<div class="post_small_date"><a href="#">December 11, 2018</a></div>
													</div>
												</div>

												<!-- Popular Post -->
												<div class="post_small d-flex flex-row align-items-center justify-content-start">
													<div class="post_small_image"><a href="#"><img src="images/recent_7.jpg" alt=""></a></div>
													<div class="post_small_content">
														<div class="post_small_title"><a href="#">Time pass atractions</a></div>
														<div class="post_small_date"><a href="#">December 05, 2018</a></div>
													</div>
												</div>

											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

						<!-- Sidebar Search -->
						<div class="sidebar_search">
							<form action="#" class="sidebar_search_form">
								<input type="text" class="sidebar_search_input" placeholder="enter keywords" required="required">
								<button class="sidebar_search_button">
									<span>
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 512 512" enable-background="new 0 0 512 512" width="512px" height="512px">
											<g>
												<path d="M495,466.2L377.2,348.4c29.2-35.6,46.8-81.2,46.8-130.9C424,103.5,331.5,11,217.5,11C103.4,11,11,103.5,11,217.5   S103.4,424,217.5,424c49.7,0,95.2-17.5,130.8-46.7L466.1,495c8,8,20.9,8,28.9,0C503,487.1,503,474.1,495,466.2z M217.5,382.9   C126.2,382.9,52,308.7,52,217.5S126.2,52,217.5,52C308.7,52,383,126.3,383,217.5S308.7,382.9,217.5,382.9z" fill="#FFFFFF"></path>
											</g>
										</svg>
									</span>
								</button>
							</form>
						</div>

						<!-- Quote of the day -->
						<div class="quote">
							<div class="sidebar_title"><h3>Quote of the day</h3></div>
							<div class="quote_text">
								<p>Sed ut perspiciatis unde omnis iste na tus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
							</div>
						</div>

						<!-- Slider -->
						<div class="sidebar_slider_section">
							<div class="sidebar_title"><h3>Slider</h3></div>
							<div class="sidebar_slider_container">
								<div class="owl-carousel owl-theme sidebar_slider">
									
									<!-- Slide -->
									<div class="slide">
										<div class="background_image" style="background-image:url(images/sidebar.jpg)"></div>
									</div>

									<!-- Slide -->
									<div class="slide">
										<div class="background_image" style="background-image:url(images/sidebar.jpg)"></div>
									</div>

									<!-- Slide -->
									<div class="slide">
										<div class="background_image" style="background-image:url(images/sidebar.jpg)"></div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

    <?php $content = ob_get_clean() ?>
<?php include 'app/views/layout.html.php' ?>