
<?php ob_start() ?>
  <!-- Editable table -->
  <div class="card">
  <div class="card-body">
    <div id="table">
    <span class="table-add float-left mb-3 mr-2"><a href="<?= ROOT_PATH ?>books/pdf"><i
            aria-hidden="true"></i>Export to PDF</a></span>

    <span class="table-add float-right mb-3 mr-2"><a href="<?= ROOT_PATH ?>books/add" class="text-success"><i
            class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>  
    <table class="table table-bordered table-responsive-md table-striped text-center">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NAME</th>
            <th class="text-center">PRICE</th>
            <th class="text-center">AUTHORS</th>
            <th class="text-center">ISBN</th>
            <th class="text-center">PUBLISHER</th>
            <th class="text-center">PUBLISHED DATE</th>
            <th class="text-center">UPDATE AT</th>
            <th class="text-center">CREATED AT</th>
            <th class="text-center">ACTIONS</th>
          </tr>
        </thead>
        <tbody>
 
        <?php        
        foreach ($data['books'] as $key ) {?>
          <tr>
          <td class="pt-3-half" ><?=$key->id ?> </td>
          <td class="pt-3-half" ><?=$key->name ?> </td>
          <td class="pt-3-half" ><?=$key->price ?> </td>
          <td class="pt-3-half" ><?=$key->authors ?> </td>
          <td class="pt-3-half" ><?=$key->isbn ?> </td>
          <td class="pt-3-half" ><?=$key->publisher ?> </td>
          <td class="pt-3-half" ><?=$key->published_date ?> </td>
          <td class="pt-3-half"><?=$key->updated_at ?> </td>
          <td class="pt-3-half" ><?=$key->created_at ?> </td>
          <td class="pt-3-half">
          <a href="<?= ROOT_PATH?>books/delete/<?= $key->id ?>">
          <i class="far fa-trash-alt fa-2x"></i></a>
          
          <a href="<?= ROOT_PATH?>books/edit/<?= $key->id ?>">
            <i class="far fa-edit fa-2x"></i></a>

          <a href="<?= ROOT_PATH?>books/read/<?= $key->id ?>">
          <i class="fas fa-eye fa-2x"></i></a>
        </td>
        </tr> 
        <?php }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>


<!-- Editable table -->

  <?php $content = ob_get_clean() ?>
  <?php include 'app/views/layout.html.php' ?>
</body>
</html>
