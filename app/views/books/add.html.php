<!-- Default form contact -->
<form enctype="multipart/form-data" method="POST" class="text-center border border-light p-5" action="">

    <p class="h4 mb-4">Add a new book</p>

    <!-- Name -->
    <input type="text" id="name" name="name" class="form-control mb-4" placeholder="Name">

    <!-- Price -->
    <input type="text" id="price" name="price" class="form-control mb-4" placeholder="Price">

    <!-- Authors -->
    <input type="text" id="authors" name="authors" class="form-control mb-4" placeholder="Authors">

    <!-- ISBN -->
    <input type="text" id="isbn" name="isbn" class="form-control mb-4" placeholder="ISBN">

    <!-- Publisher -->
    <input type="text" id="publisher" name="publisher" class="form-control mb-4" placeholder="Publisher">

    <!-- Published_date -->

    <input type="text" id="published_date" name="published_date" class="form-control mb-4" placeholder="Published Date">
    
    <!-- file to upload -->
    <label for="cover">Select file to upload:
        <input type="file" id="cover" name="cover"></label><br/>


    <!-- Send button -->
    <button class="btn btn-info btn-block" type="submit">Send</button>

</form>
<!-- Default form contact -->
<?php $content = ob_get_clean() ?>
<?php include 'app/views/layout.html.php' ?>