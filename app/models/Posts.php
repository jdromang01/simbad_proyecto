<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\File;

class Posts extends Eloquent{
    
    public function scopeAdd(){
        print_r($_POST); 
       $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
       print_r($post);
      if (!empty($post['name']) && (!empty($post['price'])) && (!empty($post['authors'])) && (!empty($post['isbn']))
        && (!empty($post['publisher']))
        && (!empty($post['published_date']))){
            if(!is_uploaded_file($_FILES['cover']['tmp_name'])){
                $error = 'There was no file uploaded';
                Messages::setMsg($error, 'error');
                return;
            }

            $uploadfile = $_FILES['cover']['tmp_name'];
            $uploadname = $_FILES['cover']['name'];
            $uploadtype = $_FILES['cover']['type'];
            $uploaddata = file_get_contents($uploadfile);
           


            //echo $files->id;
            $this->name = $post['name'];
            $this->price = $post['price'];
            $this->authors = $post['authors'];
            $this->isbn = $post['isbn'];
            $this->publisher = $post['publisher'];
            $this->published_date = $post['published_date'];
            $this->save();
            $files = new Files();
            $files->filename = $uploadname;
            $files->mimetype = $uploadtype;
            $files->filedata = $uploaddata;
            $files->books_id=$this->id;
            $files->save();

        }
    }

    public function files(){
        return $this->hasOne('Files');

    }

    public function scopeEdit($read)
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $read = $read->first();

        if (
            !empty($post['name']) && (isset($post['price'])) && (!empty($post['authors'])) && (!empty($post['isbn']))
            && (!empty($post['publisher']))
            && (!empty($post['published_date']))
        ) {
            $read->name = $post['name'];
            $read->price = $post['price'];
            $read->authors = $post['authors'];
            $read->isbn = $post['isbn'];
            $read->publisher = $post['publisher'];
            $read->published_date = $post['published_date'];
            $read->save();

            if (is_uploaded_file($_FILES['cover']['tmp_name'])) {
                $uploadfile = $_FILES['cover']['tmp_name'];
                $uploadname = $_FILES['cover']['name'];
                $uploadtype = $_FILES['cover']['type'];
                $uploaddata = file_get_contents($uploadfile);
                $files = new Files();
                $files->filename = $uploadname;
                $files->mimetype = $uploadtype;
                $files->filedata = $uploaddata;
                $files->books_id = $read->id;
                $files->save();
            }
        }
    }
}