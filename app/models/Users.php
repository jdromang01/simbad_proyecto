<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

Class Users extends Eloquent{

    public function scopeRegister()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
       
        if($post['name'] == '' || $post['email'] == '' || $post['password'] == ''){
            Messages::setMsg('Please fill in all fields', 'error');
            return;
        }

        if (
            !empty($post['name']) && (!empty($post['email'])) && (!empty($post['password'])) && (!empty($post['password1']))
    
        ) {
            $psswd = md5($post['password']);
           
           if($post['password'] == $post['password1']){
            //echo $files->id;
            $this->name = $post['name'];
            $this->email = $post['email'];
            $this->password = $psswd;
      
            $this->save();
           
           }else {
                $error = 'The passwords do not match';
                Messages::setMsg($error, 'error'); 
                return;
           }

        }else{
            $error = 'The register data is incomplete';
            Messages::setMsg($error, 'error');
            return;
        }
        }

    public function scopeLogin($query)
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $psswd = md5($post['password']);
        if (
            (!empty($post['email'])) && (!empty($post['password']))
        ) {
            $user = $query->where('email', $post['email'])->first();
            if ($user) {
                if ($psswd == $user->password) {
                    $_SESSION['is_logged_in'] = true;
                    $_SESSION['user_data'] = array(
                        "id" => $user->id,
                        "name" => $user->name,
                        "email" => $user->email,
                    );
                    if (isset($post['remember'])) {
                        $salt = rand(10000, 1000000); //numero random entre 10000 y 1000000
                        $randomNumber = $salt * $salt; //elevamos al cuadrado
                        $token = dechex($randomNumber); //pasamos a hexadecimal
                        $encrypt = sha1($token); //encriptamos en sha1
                        $user->token = $token;
                        $user->salt = $salt;
                        $user->save();
                        setcookie("remember", $user->email . ',' . $encrypt, time() + 3600 * 24 * 30, ROOT_PATH);
                    }
                } else {
                    Messages::setMsg('Incorrect Password', 'error');
                }
            } else {
                Messages::setMsg('Incorrect Login', 'error');
            }
        }
    }
}