<?php

class Controller {
    protected $controller;

    public function __construct($name){
        $this->controller = $name;
    }

    public function view($view, $data = [])
    {
        require_once 'app/views/' . $this->controller . '/' . $view . '.php';
    }

    public function remember()
    {
        if (isset($_COOKIE['remember'])) {
            $remember = explode(",", $_COOKIE['remember']);
            //remember[0] email
            $user = Users::where('email', $remember[0])->first();
            $clave = $remember[1];
            $key = sha1($user->token);
            if ($key != $clave) {
                setcookie("remember", "", time() - 3600, ROOT_PATH);
                session_destroy();
            } else {
                $_SESSION['is_logged_in'] = true;
                $_SESSION['user_data'] = array(
                    "id" => $user->id,
                    "name" => $user->name,
                    "email" => $user->email,
                );
            }
        }
    }
}