<?php

class BlogController extends Controller{
    public function index(){
        $posts = Posts::all();
        $this->view('blog.html', ['posts' => $posts]);
    }

    public function add(){
        if ($_SERVER['REQUEST_METHOD'] == 'GET'){
            $this->view('add.html');
            }else {
                Books::add();
                header('Location: ' . ROOT_PATH);
            }
    }

    public function delete($id){
        Books::destroy($id);
        header('Location: ' . ROOT_PATH);
    }

    public function read($id){
      $books =  Books::find($id);
      $this->view('read.html', ['books' => $books]);
    }

    public function edit($id){
        $books = Books::find($id);
        if ($_SERVER['REQUEST_METHOD'] == 'GET'){
            
            $this->view('edit.html', ['books' => $books]);

        }else{
            Books::read($books);
            header('Location: ' . ROOT_PATH);
        }
        

    } 


    public function pdf(){
        $books=Books::all();
        $pdf = new PDF();
        $header=array('id','name','price','authors');
        $pdf->SetFont('Arial','',14);
        $pdf->AddPage();

        $pdf->BuildTable($header,$books);
        $pdf->Output('ListBooks.pdf','D');
    }
}