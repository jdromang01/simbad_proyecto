<?php
class UsersController extends Controller{

    public function register(){
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->view('register.html');
        } else {
            Users::register();
            header("Location:" . ROOT_URL.'users/login');
        }
    }

    public function login(){
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->view('login.html');
        } else {
            Users::login();
            //Generate current date
            $date = date('d/m/Y h:i:s a', time());
            //save that date into a cookie
            setcookie("cachopowashere", $date, time() + 3600 * 24, ROOT_PATH);

            header("Location:" . ROOT_URL);
        }
    }


    public function logout(){
        if (isset($_SESSION['is_logged_in'])) {
            setcookie("remember", "", time() - 3600, ROOT_PATH);
            session_destroy();
            header("Location:" . ROOT_URL);
        }
	}









}