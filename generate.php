<?php

$srcurl = 'http://localhost:90/DWES/simbad/';
$tempfilename = $_SERVER['DOCUMENT_ROOT'] . 
    '/DWES/simbad/tempindex.html';
$targetfilename = $_SERVER['DOCUMENT_ROOT'] .
    '/DWES/simbad/index.html';

    if(file_exists($tempfilename)){
        unlink($tempfilename);
    }

    $html = file_get_contents($srcurl);
    if (!html){
        $error = "Unable to load $srcurl. Static page update aborted!";
        echo $error;
        exit();
    }

    if (!file_put_contents($tempfilename, $html)){
        $error = "Unable to write $tempfilename. Static page update aborted";
        echo $error;
        exit();
    }

    copy($tempfilename, $targetfilename);
    unlink($tempfilename);
    echo "Home page generated successfully!";